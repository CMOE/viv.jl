module MeshGeneration

using GridapGmsh: gmsh, GmshDiscreteModel
using Gridap
using DrWatson

function create_mesh(L,H,D,Cx,Cy,      # Domain
  h_coarse,h_fine,dxLeft,dxRight,dyTop,dyBottom,
  decay_factor_left,decay_factor_right,decay_exponent_left,decay_exponent_right,
  filename) # Mesh

  # Domain Parameters
  R = D/2

  # Initialize
  gmsh.initialize()
  gmsh.option.setNumber("General.Terminal", 1)
  gmsh.model.add("t1")

  # Cylinder
  disk = gmsh.model.occ.addDisk(Cx,Cy,0,R,R,2)
  println(disk)

  # Background Domain
  rectangle = gmsh.model.occ.addRectangle(0,0,0,L,H)
  println(rectangle)

  # Subtract cylinder from Background
  domain = gmsh.model.occ.cut((2,rectangle),(2,disk),-1,true,true)
  domain = domain[1][end][end]

  # Get entities
  fluid_domain = gmsh.model.occ.getEntitiesInBoundingBox(-0.1,-0.1,-0.1,L+0.1,H+0.1,0.1,2)
  wall1 = gmsh.model.occ.getEntitiesInBoundingBox(-0.1,H-0.1,-0.1,L+0.1,H+0.1,0.1,1)
  wall2 = gmsh.model.occ.getEntitiesInBoundingBox(-0.1,-0.1,-0.1,L+0.1,0.1,0.1,1)
  inlet_point = gmsh.model.occ.getEntitiesInBoundingBox(-0.1,-0.1,-0.1,0.1,H+0.1,0.1,0)
  inlet_line = gmsh.model.occ.getEntitiesInBoundingBox(-0.1,-0.1,-0.1,0.1,H+0.1,0.1,1)
  outlet_point = gmsh.model.occ.getEntitiesInBoundingBox(L-0.1,-0.1,-0.1,L+0.1,H+0.1,0.1,0)
  outlet_line = gmsh.model.occ.getEntitiesInBoundingBox(L-0.1,-0.1,-0.1,L+0.1,H+0.1,0.1,1)
  cylinder_point = gmsh.model.occ.getEntitiesInBoundingBox(Cx-R-0.1,Cy-R-0.1,-0.1,Cx+R+0.1,Cy+R+0.1,0.1,0)
  cylinder_line = gmsh.model.occ.getEntitiesInBoundingBox(Cx-R-0.1,Cy-R-0.1,-0.1,Cx+R+0.1,Cy+R+0.1,0.1,1)


  # Get entity tags
  fluid_domain_tags = [ entity[2] for entity in fluid_domain]
  wall_tags = [ entity[2] for entity in wall1]
  append!(wall_tags , [ entity[2] for entity in wall2])
  inlet_point_tags = [ entity[2] for entity in inlet_point]
  inlet_line_tags = [ entity[2] for entity in inlet_line]
  outlet_point_tags = [ entity[2] for entity in outlet_point]
  outlet_line_tags = [ entity[2] for entity in outlet_line]
  cylinder_point_tags = [ entity[2] for entity in cylinder_point]
  cylinder_line_tags = [ entity[2] for entity in cylinder_line]

  println("fluid_domain", fluid_domain)

  # Physical group
  gmsh.model.addPhysicalGroup(0,inlet_point_tags,1,"inlet")
  gmsh.model.addPhysicalGroup(1,inlet_line_tags,1,"inlet")
  gmsh.model.addPhysicalGroup(0,outlet_point_tags,2,"walls")
  gmsh.model.addPhysicalGroup(1,outlet_line_tags,2,"outlet")
  gmsh.model.addPhysicalGroup(1,wall_tags,3,"walls")
  gmsh.model.addPhysicalGroup(0,cylinder_point_tags,4,"cylinderWalls")
  gmsh.model.addPhysicalGroup(1,cylinder_line_tags,4,"cylinderWalls")
  pg1 = gmsh.model.addPhysicalGroup(2,fluid_domain_tags)#,5,"fluid")
  gmsh.model.setPhysicalName(2,pg1,"fluid")

  # Synchronize
  gmsh.model.occ.synchronize()
  gmsh.model.geo.synchronize()

  # Define mesh size
  function meshSizeCallback(dim,tag,x,y,z,lc)
    if (Cx-R-dxLeft)<x<(Cx) && √((x-Cx)^2+(y-Cy)^2) < (R+dxLeft)
      dist = abs(√((x-Cx)^2+(y-Cy)^2) - R)/R
      return min(h_fine * (1 + decay_factor_left * (dist^decay_exponent_left)), h_coarse)
    elseif (Cx)<x<(Cx+R+dxRight) && (Cy-R-dyBottom)<y<(Cy+R+dyTop)
      dist = abs(√((x-Cx)^2+(y-Cy)^2) - R)/R
      return min(h_fine * (1 + decay_factor_right * (dist^decay_exponent_right)), h_coarse)
    else
      return h_coarse
    end
  end
  gmsh.model.mesh.setSizeCallback(meshSizeCallback)
  gmsh.write("tmp.geo_unrolled")
  gmsh.model.mesh.generate()

  println(gmsh.model.getEntitiesForPhysicalGroup(2,5))

  # Finalize
  gmsh.write(filename)
  gmsh.finalize()

end

# Domain Parameters
L = 2.2
H = 0.41
D = 0.1
R = D/2
Cx = 0.15 + R #L/2-2R
Cy = 0.15 + R

# Coarse Mesh Parameters
h_coarse = 1.0
h_fine = 1.0
dxLeft = R
dxRight = 12R
dyTop = R
dyBottom = R
decay_factor_left = 5
decay_factor_right = 1.5
decay_exponent_left = 0.8
decay_exponent_right = 0.8

filename = datadir("meshes","cylinder_coarse.msh")
create_mesh(L, H, D, Cx, Cy,
  h_coarse, h_fine, dxLeft, dxRight, dyTop, dyBottom,
  decay_factor_left, decay_factor_right, decay_exponent_left, decay_exponent_right,
  filename)
model =  GmshDiscreteModel(filename)

# Medium Mesh Parameters
h_coarse = 0.1
h_fine = 1e-2
dxLeft = R
dxRight = 12R
dyTop = R
dyBottom = R
decay_factor_left = 5
decay_factor_right = 1.5
decay_exponent_left = 0.8
decay_exponent_right = 0.8

filename = datadir("meshes","cylinder_medium.msh")
create_mesh(L, H, D, Cx, Cy,
  h_coarse, h_fine, dxLeft, dxRight, dyTop, dyBottom,
  decay_factor_left, decay_factor_right, decay_exponent_left, decay_exponent_right,
  filename)
model =  GmshDiscreteModel(filename)

# Fine Mesh Parameters
h_coarse = 5.0e-2
h_fine = 1.0e-2
dxLeft = 2R
dxRight = 6R
dyTop = R
dyBottom = R
decay_factor_left = 5
decay_factor_right = 5
decay_exponent_left = 0.8
decay_exponent_right = 0.8

filename = datadir("meshes","cylinder_fine.msh")
create_mesh(L, H, D, Cx, Cy,
  h_coarse, h_fine, dxLeft, dxRight, dyTop, dyBottom,
  decay_factor_left, decay_factor_right, decay_exponent_left, decay_exponent_right,
  filename)
model =  GmshDiscreteModel(filename)


end
