using DrWatson
using TimerOutputs
@quickactivate "VIV.jl"

# Initialize timer
const to = TimerOutput()

# Here you may include files from the source directory
include(srcdir("VIV.jl"))

# Warm-up run
mesh_file = datadir("meshes","cylinder_coarse.msh")
params = VIV.VIV_params(mesh_file=mesh_file)
@timeit to "run-coarse" VIV.main(params)

# Production run
# mesh_file = datadir("meshes","cylinder_medium.msh")
# params = VIV.VIV_params(
#   mesh_file=mesh_file,
#   Re=20,
#   Δt=0.1,
#   tf=5,
# )
# Fx,Fy = VIV.main(params)

# Fine mesh
outFolder = "sims_2D2_4500"
mesh_file= datadir("meshes", "cylinder_fine_4500.msh")
params = VIV.VIV_params(
  mesh_file=mesh_file,
  Re = 100,
  Δt=0.004,
  ΔtOut=0.100,
  tf=25.0,
  Uₘ=1.5,
  outFolder = outFolder
)
Fx,Fy,CD,CL = VIV.main(params)

data = Dict(
  "Fx" => Fx,
  "Fy" => Fy,
  "CD" => CD,
  "CL" => CL)

filename = datadir(outFolder,"force_data.jld2")
wsave(filename, data)
# Fx,Fy = @timeit to "run-medium" VIV.main(params)

# show(to)

# params = VIV.VIV_params(mesh_file="fine_mesh.msh",Δt=0.01, tf=10.0)
# Fx,Fy = VIV.main(params)
