#!/bin/sh
#
#SBATCH --job-name="VIV"
#SBATCH --partition=thin
#SBATCH --time=2-00:00:00
#SBATCH -n 4
#SBATCH -o stdout/slurm-%j-%4t.out
#SBATCH -e stdout/slurm-%j-%4t.err

source ../compile/modules_snellius.sh
mpiexecjl --project=../ -n 4 julia -J ../VIV.so -O3 --check-bounds=no -e 'include("run_VIV_snellius.jl")'
