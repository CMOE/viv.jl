using DrWatson
using TimerOutputs
@quickactivate "VIV.jl"

# Initialize timer
const to = TimerOutput()

# Here you may include files from the source directory
include(srcdir("VIV.jl"))

# Warm-up run
mesh_file = datadir("meshes","cylinder_coarse.msh")
params = VIV.VIV_params(mesh_file=mesh_file)
@timeit to "run-coarse" VIV.main(params)

# Production run
# mesh_file = datadir("meshes","cylinder_medium.msh")
# params = VIV.VIV_params(
#   mesh_file=mesh_file,
#   Re=20,
#   Δt=0.1,
#   tf=5,
# )
# Fx,Fy = VIV.main(params)

# Fine mesh
mesh_file= datadir("meshes", "cylinder_fine.msh")
params = VIV.VIV_params(
  mesh_file=mesh_file,
  Re = 20,
  Δt=0.1,
  tf=1.0,
  Uₘ=0.3,
)
@timeit to "run-fine" Fx,Fy,CD,CL = VIV.main(params)
# Fx,Fy = @timeit to "run-medium" VIV.main(params)

show(to)

# params = VIV.VIV_params(mesh_file="fine_mesh.msh",Δt=0.01, tf=10.0)
# Fx,Fy = VIV.main(params)
