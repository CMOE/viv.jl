using MPI

MPI.Init()
comm = MPI.COMM_WORLD

using VIV
using DrWatson
@quickactivate "VIV.jl"

mesh_file = datadir("meshes","cylinder_fine_0.8m_L60.msh")
params = VIV.VIV_params(
  mesh_file=mesh_file,
  test_name="08m_L60",
  exec_path="/gpfs/scratch1/nodespecific/int6/"*ENV["USER"]*"/VIV.jl/VTKs/08m_L60",
  Re = 1e3,
  Δt=0.001,
  ΔtOut=0.001,
  tf=2,
  Uₘ=15,
)
np = MPI.Comm_size(comm)
VIV.run_case(np,params)

MPI.Finalize()
