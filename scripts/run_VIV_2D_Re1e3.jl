using DrWatson
using TimerOutputs
@quickactivate "VIV"

# Initialize timer
const to = TimerOutput()

# Here you may include files from the source directory
include(srcdir("VIV.jl"))

# Warm-up run
mesh_file = datadir("meshes","cylinder_coarse.msh")
params = VIV.VIV_params(mesh_file=mesh_file)
@timeit to "run-coarse" VIV.main(params)

# Production run
# mesh_file = datadir("meshes","cylinder_medium.msh")
# params = VIV.VIV_params(
#   mesh_file=mesh_file,
#   Re=20,
#   Δt=0.1,
#   tf=5,
# )
# Fx,Fy = VIV.main(params)

# Fine mesh
outFolder = "sims_2D_Re1e3"
mesh_file= datadir("meshes", "cylinder_fine_0.8m_L60.msh")
params = VIV.VIV_params(
  mesh_file=mesh_file,
  Re = 1.0e4,
  Δt=0.05,
  ΔtOut=0.05,
  tf=0.5,
  H=0.8,
  outFolder = outFolder
)
Fx,Fy,CD,CL = VIV.main(params)

data = Dict(
  "Fx" => Fx,
  "Fy" => Fy,
  "CD" => CD,
  "CL" => CL)

filename = datadir(outFolder,"force_data.jld2")
wsave(filename, data)
# Fx,Fy = @timeit to "run-medium" VIV.main(params)

# show(to)

# params = VIV.VIV_params(mesh_file="fine_mesh.msh",Δt=0.01, tf=10.0)
# Fx,Fy = VIV.main(params)
