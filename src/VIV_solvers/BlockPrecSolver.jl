

struct BlockPrecSolver <: LinearSolver
  gmres::LinearSolver
  function BlockPrecSolver(;
                           M_ls=BackslashSolver(),
                           K_ls=BackslashSolver(),
                           gmres_iter=20,
                           atol=1e-12,
                           rtol=1e-6,
                           verbose=false)
    sc_ls = SchurComplementSolver(M_ls,K_ls)
    gmres = GMRESSolver(gmres_iter,sc_ls,atol=atol,rtol=rtol,verbose=verbose)
    new(gmres)
  end
end

struct BlockPrecSymbolicSetup <: SymbolicSetup
  solver
  gmres_ss::GMRESSymbolicSetup
end

mutable struct BlockPrecNumericalSetup <: NumericalSetup
  solver
  gmres_ns::NumericalSetup
end

function symbolic_setup(ls::BlockPrecSolver,A::AbstractMatrix)
  gmres_ss = symbolic_setup(ls.gmres,A)
  BlockPrecSymbolicSetup(ls,gmres_ss)
end

function numerical_setup(ss::BlockPrecSymbolicSetup,A::AbstractMatrix)
  gmres_ns = numerical_setup(ss.gmres_ss,A)
  BlockPrecNumericalSetup(ss.solver,gmres_ns)
end

function numerical_setup!(ns::BlockPrecNumericalSetup,A::AbstractMatrix)
  numerical_setup!(ns.gmres_ns,A)
  ns
end

function solve!(x::AbstractBlockVector,ns::BlockPrecNumericalSetup,b::AbstractBlockVector)
  # gmres!(x,ns.gmres_ns.A,b,Pl=ns.gmres_ns.Pl_ns)
  fill!(x,zero(eltype(x)))
  solve!(x,ns.gmres_ns,b)
end
