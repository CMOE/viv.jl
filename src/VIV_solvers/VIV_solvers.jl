module VIVSolvers

using Gridap
using Gridap.Algebra
using Gridap.Algebra: mul!
using Gridap.Helpers
using LinearAlgebra
using BlockArrays
using TimerOutputs

import Gridap.Algebra: symbolic_setup
import Gridap.Algebra: numerical_setup, numerical_setup!
import Gridap.Algebra: solve!

export BlockPrecSolver

include("SchurComplementSolver.jl")
include("GMRESSolver.jl")
include("BlockPrecSolver.jl")

end # module
