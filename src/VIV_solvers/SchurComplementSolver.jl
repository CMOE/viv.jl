"""
  Schur complement solver

  [A B] ^ -1    [Ip  -A^-1 B]   [A^-1     ]   [   Ip       ]
  [C D]       = [       Iq  ] ⋅ [     S^-1] ⋅ [-C A^-1   Iq]

  where S = D - C A^-1 B
"""
struct SchurComplementSolver <: LinearSolver
  A_ls::LinearSolver
  S_ls::LinearSolver
end
# struct SchurComplementSolver{T1,T2,T3,T4} <: LinearSolver
#   A :: T1
#   B :: T2
#   C :: T3
#   S :: T4
#   function SchurComplementSolver(A::Gridap.Algebra.NumericalSetup,
#                                  B::AbstractMatrix,
#                                  C::AbstractMatrix,
#                                  S::Gridap.Algebra.NumericalSetup)
#     T1 = typeof(A)
#     T2 = typeof(B)
#     T3 = typeof(C)
#     T4 = typeof(S)
#     return new{T1,T2,T3,T4}(A,B,C,S)
#   end
# end

struct SchurComplementSymbolicSetup <: SymbolicSetup
  solver
  A_ss::SymbolicSetup
  S_ss::SymbolicSetup
end

function symbolic_setup(solver::SchurComplementSolver,mat::AbstractMatrix)
  A = mat[Block(1,1)]
  S = mat[Block(2,2)]
  A_ss = symbolic_setup(solver.A_ls,A)
  S_ss = symbolic_setup(solver.S_ls,S)
  SchurComplementSymbolicSetup(solver,A_ss,S_ss)
end

mutable struct SchurComplementNumericalSetup{T1,T2,T3,T4} <: NumericalSetup
  solver
  A_ns :: T1
  B :: T2
  C :: T3
  S_ns :: T4
  caches
  function SchurComplementNumericalSetup(solver::SchurComplementSolver,
                                 A_ns::Gridap.Algebra.NumericalSetup,
                                 B::AbstractMatrix,
                                 C::AbstractMatrix,
                                 S_ns::Gridap.Algebra.NumericalSetup)
    T1 = typeof(A_ns)
    T2 = typeof(B)
    T3 = typeof(C)
    T4 = typeof(S_ns)
    caches = get_shur_complement_caches(B,C)
    return new{T1,T2,T3,T4}(solver,A_ns,B,C,S_ns,caches)
  end
end

function get_shur_complement_caches(B::AbstractMatrix,C::AbstractMatrix)
  du = allocate_col_vector(C)
  bu = allocate_col_vector(C)
  bp = allocate_col_vector(B)
  return du,bu,bp
end

function numerical_setup(ss::SchurComplementSymbolicSetup,mat::AbstractMatrix)
  A = mat[Block(1,1)]
  B = mat[Block(1,2)]
  C = mat[Block(2,1)]
  S = mat[Block(2,2)]
  A_ns = numerical_setup(ss.A_ss,A)
  S_ns = numerical_setup(ss.S_ss,S)
  return SchurComplementNumericalSetup(ss.solver,A_ns,B,C,S_ns)
end

function numerical_setup!(ns::SchurComplementNumericalSetup,mat::AbstractMatrix)
  A = mat[Block(1,1)]
  S = mat[Block(2,2)]
  numerical_setup!(ns.A_ns,A)
  numerical_setup!(ns.S_ns,S)
  ns.B = mat[Block(1,2)]
  ns.C = mat[Block(2,1)]
  ns
end

function solve!(x::AbstractBlockVector,ns::SchurComplementNumericalSetup,y::AbstractBlockVector)
  A,B,C,S = ns.A_ns,ns.B,ns.C,ns.S_ns
  du,bu,bp = ns.caches

  @check blocklength(x) == blocklength(y) == 2
  y_u = y[Block(1)]; y_p = y[Block(2)]
  x_u = x[Block(1)]; x_p = x[Block(2)]

  # # Solve Schur complement (Full prec)
  # solve!(x_u,A,y_u)                      # x_u = A^-1 y_u
  # copy!(bp,y_p); mul!(bp,C,x_u,-1.0,1.0)  # bp  = y_p - C*(A^-1 y_u)
  # solve!(x_p,S,bp)                       # x_p = S^-1 bp

  # mul!(bu,B,x_p)         # bu  = B*x_p
  # solve!(du,A,bu)        # du = A^-1 bu
  # x_u .-= du             # x_u  = x_u - du

  # # Solve Schur complement (Diagonal prec)
  # solve!(x_u,A,y_u)                      # x_u = A^-1 y_u
  # solve!(x_p,S,y_p)                       # x_p = S^-1 yp

  # Solve Schur complement (Lower prec)
  solve!(x_u,A,y_u)                       # x_u = A^-1 y_u
  copy!(bp,y_p); mul!(bp,C,x_u,-1.0,1.0)  # bp  = y_p - C*(A^-1 y_u)
  solve!(x_p,S,bp)                        # x_p = S^-1 bp

  return x
end
