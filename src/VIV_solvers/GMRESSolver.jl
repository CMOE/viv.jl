
# Row/Col vector allocations for serial
function allocate_row_vector(A::AbstractMatrix{T}) where T
  return zeros(T,size(A,1))
end

function allocate_col_vector(A::AbstractMatrix{T}) where T
  return zeros(T,size(A,2))
end

# Row/Col vector allocations for blocks
function allocate_row_vector(A::AbstractBlockMatrix)
  return mortar(map(Aii->allocate_row_vector(Aii),blocks(A)[:,1]))
end

function allocate_col_vector(A::AbstractBlockMatrix)
  return mortar(map(Aii->allocate_col_vector(Aii),blocks(A)[1,:]))
end

# GMRES Solver
struct GMRESSolver <: Gridap.Algebra.LinearSolver
  m   ::Int
  Pl  ::Gridap.Algebra.LinearSolver
  atol::Float64
  rtol::Float64
  verbose::Bool
end

function GMRESSolver(m,Pl;atol=1e-12,rtol=1.e-6,verbose=false)
  return GMRESSolver(m,Pl,atol,rtol,verbose)
end

struct GMRESSymbolicSetup <: Gridap.Algebra.SymbolicSetup
  solver
  Pl_ss
end

function symbolic_setup(solver::GMRESSolver, A::AbstractMatrix)
  Pl_ss = symbolic_setup(solver.Pl,A)
  return GMRESSymbolicSetup(solver,Pl_ss)
end

mutable struct GMRESNumericalSetup <: Gridap.Algebra.NumericalSetup
  solver
  Pl_ns
  A
  caches
end

function get_gmres_caches(m,A)
  w = allocate_col_vector(A)
  V = [allocate_col_vector(A) for i in 1:m+1]
  Z = [allocate_col_vector(A) for i in 1:m]

  H = zeros(m+1,m)  # Hessenberg matrix
  g = zeros(m+1)    # Residual vector
  c = zeros(m)      # Gibens rotation cosines
  s = zeros(m)      # Gibens rotation sines
  return (w,V,Z,H,g,c,s)
end

function numerical_setup(ss::GMRESSymbolicSetup, A::AbstractMatrix)
  solver = ss.solver
  Pl_ns  = numerical_setup(ss.Pl_ss,A)
  caches = get_gmres_caches(solver.m,A)
  return GMRESNumericalSetup(solver,Pl_ns,A,caches)
end

function numerical_setup!(ns::GMRESNumericalSetup, A::AbstractMatrix)
  numerical_setup!(ns.Pl_ns,A)
  ns.A = A
  ns
end

function solve!(x::AbstractVector,ns::GMRESNumericalSetup,b::AbstractVector)
  solver, A, Pl, caches = ns.solver, ns.A, ns.Pl_ns, ns.caches
  m, atol, rtol, verbose = solver.m, solver.atol, solver.rtol, solver.verbose
  w, V, Z, H, g, c, s = caches
  verbose && println(" > Starting GMRES solver: ")

  # Initial residual
  # mul!(w,A,x);
  fill!(w,zero(eltype(w)))
  for k in 1:blocksize(A,1)
    for l in 1:blocksize(A,2)
      mul!(view(w,Block(k)),view(A,Block(k,l)),view(x,Block(l)),1,1)
    end
  end
  w .= b .- w

  β    = norm(w); β0 = β
  converged = (β < atol || β < rtol*β0)
  iter = 0
  while !converged
    verbose && println("   > Iteration ", iter," - Residual: ", β)
    fill!(H,0.0)

    # Arnoldi process
    fill!(g,0.0); g[1] = β
    V[1] .= w ./ β
    j = 1
    while ( j < m+1 && !converged )
      verbose && println("      > Inner iteration ", j," - Residual: ", β)
      # Arnoldi orthogonalization by Modified Gram-Schmidt
      solve!(Z[j],Pl,V[j])
      # mul!(w,A,Z[j])
      for k in 1:blocksize(A,1)
        for l in 1:blocksize(A,2)
          mul!(view(w,Block(k)),view(A,Block(k,l)),view(Z[j],Block(l)),1,1)
        end
      end
      for i in 1:j
        H[i,j] = dot(w,V[i])
        w .= w .- H[i,j] .* V[i]
      end
      H[j+1,j] = norm(w)
      V[j+1] = w ./ H[j+1,j]

      # Update QR
      for i in 1:j-1
        γ = c[i]*H[i,j] + s[i]*H[i+1,j]
        H[i+1,j] = -s[i]*H[i,j] + c[i]*H[i+1,j]
        H[i,j] = γ
      end

      # New Givens rotation, update QR and residual
      c[j], s[j], _ = LinearAlgebra.givensAlgorithm(H[j,j],H[j+1,j])
      H[j,j] = c[j]*H[j,j] + s[j]*H[j+1,j]; H[j+1,j] = 0.0
      g[j+1] = -s[j]*g[j]; g[j] = c[j]*g[j]

      β  = abs(g[j+1]); converged = (β < atol || β < rtol*β0)
      j += 1
    end
    j = j-1

    # Solve least squares problem Hy = g by backward substitution
    for i in j:-1:1
      g[i] = (g[i] - dot(H[i,i+1:j],g[i+1:j])) / H[i,i]
    end

    # Update solution & residual
    for i in 1:j
      x .+= g[i] .* Z[i]
    end
    # mul!(w,A,x)
    for k in 1:blocksize(A,1)
      for l in 1:blocksize(A,2)
        mul!(view(w,Block(k)),view(A,Block(k,l)),view(x,Block(l)),1,1)
      end
    end
    w .= b .- w

    iter += 1
  end
  verbose && println("   > Num Iter: ", iter," - Final residual: ", β)
  verbose && println("   Exiting GMRES solver.")

  return x
end
