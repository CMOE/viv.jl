# Setup solver via low level PETSC API calls
function Stokes_ksp_setup(ksp)
  pc       = Ref{GridapPETSc.PETSC.PC}()
  mumpsmat = Ref{GridapPETSc.PETSC.Mat}()
  @check_error_code GridapPETSc.PETSC.KSPSetType(ksp[],GridapPETSc.PETSC.KSPPREONLY)
  @check_error_code GridapPETSc.PETSC.KSPGetPC(ksp[],pc)
  @check_error_code GridapPETSc.PETSC.PCSetType(pc[],GridapPETSc.PETSC.PCLU)
  @check_error_code GridapPETSc.PETSC.PCFactorSetMatSolverType(pc[],GridapPETSc.PETSC.MATSOLVERMUMPS)
  @check_error_code GridapPETSc.PETSC.PCFactorSetUpMatSolverType(pc[])
  @check_error_code GridapPETSc.PETSC.PCFactorGetMatrix(pc[],mumpsmat)
  @check_error_code GridapPETSc.PETSC.MatMumpsSetIcntl(mumpsmat[],  4, 2)
  @check_error_code GridapPETSc.PETSC.MatMumpsSetIcntl(mumpsmat[],  7, 0)
  @check_error_code GridapPETSc.PETSC.MatMumpsSetIcntl(mumpsmat[],  14, 5000)
  @check_error_code GridapPETSc.PETSC.MatMumpsSetIcntl(mumpsmat[],  24, 1)
  # @check_error_code GridapPETSc.PETSC.MatMumpsSetIcntl(mumpsmat[], 28, 2)
  # @check_error_code GridapPETSc.PETSC.MatMumpsSetIcntl(mumpsmat[], 29, 2)
  @check_error_code GridapPETSc.PETSC.MatMumpsSetCntl(mumpsmat[], 3, 1.0e-10)
  @check_error_code GridapPETSc.PETSC.KSPSetFromOptions(ksp[])
end

"""
main()

Main function for VIV_Parallel.jl. This function is called when the module is run as a script.
In this function we solve the Navier-Stokes equations for an oscillating cylinder in
cross-flow within a channel using a parallel solver.

# Arguments
- `params::VIV_params`: Parameters for the simulation.
- `ranks`: MPI-communicator ranks

#Output
- time trace of forces on the cylinder
"""
function main(params::VIV_params,ranks)

  # Auxiliar functions for logging
  @unpack test_name = params
  if i_am_main(ranks)
    log_filename = datadir(test_name,"output.log")
    io = open(log_filename, "w")
    force_filename = datadir(test_name,"force_data.txt")
    io_force = open(force_filename, "w")
  end
  function to_logfile(x...)
    if i_am_main(ranks)
      write(io,join(x, " ")...)
      write(io,"\n")
      flush(io)
    end
  end
  function to_forcefile(x...)
    if i_am_main(ranks)
      write(io_force,join(x, " ")...)
      write(io_force,"\n")
      flush(io_force)
    end
  end

  # Load mesh
  to_logfile("Mesh")
  @unpack mesh_file = params
  @unpack H,L = params
  𝒯 = GmshDiscreteModel(ranks,mesh_file)

  # Triangulations
  Ω = Interior(𝒯)
  Γout = Boundary(𝒯,tags="outlet")
  Γcyl = Boundary(𝒯,tags="cylinderWalls")
  Γside = Boundary(𝒯,tags="walls")
  ncyl = get_normal_vector(Γcyl)
  nout = get_normal_vector(Γout)

  # Boundary conditions
  @unpack Re,D,ν,Uₘ = params
  @show U∞ = Re*ν/D
  @show Re
  u₀(x,t) = VectorValue(0.0,0.0)
  u₀(t::Real) = x->u₀(x,t)
  uin((x,y),t) = VectorValue( 4 * Uₘ * y * ( H - y ) / ( (H^2) ), 0.0 ) #VectorValue(U∞,0.0)
  uin(t::Real) = x -> uin(x,t)

  # Define FE spaces
  to_logfile("FE Spaces")
  @unpack order = params
  reffeᵤ = ReferenceFE(lagrangian,VectorValue{2,Float64},order)
  reffeₚ = ReferenceFE(lagrangian,Float64,order-1)
  V = TestFESpace(Ω,reffeᵤ, conformity=:H1,
    dirichlet_tags=["inlet","walls","cylinderWalls"],
    dirichlet_masks=[(true,true),(true,true),(true,true)])
  U = TransientTrialFESpace(V, [uin,u₀,u₀])
  Q = TestFESpace(Ω,reffeₚ, conformity=:C0)
  P = TrialFESpace(Q)
  Κ = TestFESpace(Ω,reffeᵤ, conformity=:H1,
    dirichlet_tags=["inlet","walls","cylinderWalls"],
    dirichlet_masks=[(true,true),(true,true),(true,true)])
  Η = TrialFESpace(Κ,[u₀(0),u₀(0),u₀(0)])
  X = TransientMultiFieldFESpace([U,P,Η])
  Y = MultiFieldFESpace([V,Q,Κ])
  X₀ = MultiFieldFESpace([U(0.0),P])
  Y₀ = MultiFieldFESpace([V,Q])

  # Measures
  degree = 2*order
  dΩ = Measure(Ω,degree)
  dΓcyl = Measure(Γcyl,degree)
  dΓout = Measure(Γout,degree)

  # Auxiliar forms
  c(a,u,v) = 0.5*((a⋅∇(u))⋅v - (a⋅∇(v))⋅u)

  # Stabilization Parameters
  c₁ = 4.0
  c₂ = 2.0
  cc = 1.0
  h2map = map(Ω.trians) do trian
    CellField(get_cell_measure(trian),trian)
  end
  h2 = DistributedCellField(h2map)
  hmap = map(Ω.trians) do trian
    CellField(lazy_map(dx->dx^(1/2),get_cell_measure(trian)),trian)
  end
  h = DistributedCellField(hmap)
  τₘ(u) = 1/(c₁*ν/h2 + c₂*((u⋅u).^(1/2))/h)
  τc(u) = cc * (h2/(c₁*τₘ(u)))

  # Define FE Operators
  a₀((u,p),(v,q)) = ∫( ν*(ε(u)⊙ε(v)) + (∇⋅u)*q - (∇⋅v)*p )dΩ
  l₀((v,q)) = ∫( 0.0*q )dΩ
  res(t,(u,p,η),(v,q,κ)) =
    ∫( ∂t(u)⋅v + 2ν*(ε(u)⊙ε(v)) + c(u,u,v) + (∇⋅u)*q - (∇⋅v)*p +
      τₘ(u)*((∇(u)'⋅u - η)⋅(∇(v)'⋅u - κ)) + τc(u)*((∇⋅u)*(∇⋅v)) )dΩ +
    ∫( 0.5*(u⋅nout)*(u⋅v) )dΓout
  jac(t,(u,p,η),(du,dp,dη),(v,q,κ)) =
    ∫( 2ν*(ε(du)⊙ε(v)) + c(du,u,v) + c(u,du,v) + (∇⋅du)*q - (∇⋅v)*dp +
      τₘ(u)*((∇(du)'⋅u + ∇(u)'⋅du - dη)⋅(∇(v)'⋅u - κ)) + τₘ(u)*((∇(u)'⋅u - η)⋅(∇(v)'⋅du)) +
      τₘ(du)*((∇(u)'⋅u - η)⋅(∇(v)'⋅u - κ)) + τc(u)*((∇⋅du)*(∇⋅v)) + τc(du)*((∇⋅u)*(∇⋅v)) )dΩ +
    ∫( 0.5*((du⋅nout)*(u⋅v)+(u⋅nout)*(du⋅v)) )dΓout
  jac_t(t,(u,p,η),(dut,),(v,q,κ)) = ∫( dut⋅v )dΩ
  op₀ = AffineFEOperator(a₀,l₀,X₀,Y₀)
  op = TransientFEOperator(res,jac,jac_t,X,Y)

  # Initial solution
  to_logfile("Stokes solve for initial solution")
  ls₀ = PETScLinearSolver(Stokes_ksp_setup)
  uₕₒ, pₕₒ = solve(ls₀,op₀)
  xₕ₀ = interpolate_everywhere([u₀(0.0),0.0,u₀(0.0)],X(0.0))#interpolate_everywhere([uₕₒ,pₕₒ,u₀(0.0)],X(0.0))
  ∂ₜxₕ₀ = interpolate_everywhere([u₀(0.0),0.0,u₀(0.0)],X(0.0))
  writevtk(Ω,"sol0",cellfields=["u"=>xₕ₀[1],"p"=>xₕ₀[2]],order=order)

  # Solver
  @unpack Δt,tf,ΔtOut,ρ∞ = params
  nls = PETScNonlinearSolver()
  # nls = NLSolver(show_trace=true,method=:newton,iterations=3)
  ode_solver = GeneralizedAlpha(nls,Δt,ρ∞)
  # ode_solver = ThetaMethod(nls,Δt,1.0)

  # solution
  xₕₜ = solve(ode_solver,op,(xₕ₀,∂ₜxₕ₀),0.0,tf)
  # xₕₜ = solve(ode_solver,op,xₕ₀,0.0,tf)

  # Post-processing
  to_logfile("Navier-Stokes solution")
  Fx = Float64[]
  Fy = Float64[]
  CD = Float64[]
  CL = Float64[]
  outMod = ceil(Int64, ΔtOut / Δt)
  if (i_am_main(ranks))
    tick()
  end
  createpvd(ranks,"sol") do pvd
    cnt = 0
    for ((uₕ,pₕ,ηₕ),t) in xₕₜ
      cnt += 1
      Fxₜ,Fyₜ = ∑(∫(pₕ*ncyl-2ν*(ε(uₕ)⋅ncyl))dΓcyl)
      push!(Fx,Fxₜ)
      CDₜ = Fxₜ / (0.5 * D * U∞^2)
      push!(CD,CDₜ)
      push!(Fy,Fyₜ)
      CLₜ = Fyₜ  / (0.5 * D * U∞^2)
      push!(CL,CLₜ)
      to_logfile("Count = $cnt, t = $t, Fx = $Fxₜ, Fy = $Fyₜ, CD = $CDₜ, CL = $CLₜ")
      to_forcefile("Count = $cnt, t = $t, Fx = $Fxₜ, Fy = $Fyₜ, CD = $CDₜ, CL = $CLₜ")

      if(cnt%outMod == 0)
        pvd[t] = createvtk(Ω,"sol_"*@sprintf("%.5f",t)*".vtu",
          cellfields=["u"=>uₕ,"p"=>pₕ,"eta"=>ηₕ],order=order)
      end

      if (i_am_main(ranks))
        tock()
        to_logfile("----------\n")
        tick()
      end
    end
  end

  if i_am_main(ranks)
    close(io)
    close(io_force)
    tock()
  end

  return Fx,Fy,CD,CL

end
