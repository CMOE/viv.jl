"""
main()

Main function for VIV.jl. This function is called when the module is run as a script.
In this function we solve the Navier-Stokes equations for an oscillating cylinder in
cross-flow within a channel.

# Arguments
- `params::VIV_params`: Parameters for the simulation.

#Output
- time trace of forces on the cylinder
"""
function main(params)

  # Load mesh
  @unpack mesh_file, outFolder = params
  @unpack H,L = params
  𝒯 = GmshDiscreteModel(mesh_file)

  # Triangulations
  Ω = Interior(𝒯)
  Γout = Boundary(𝒯,tags="outlet")
  Γcyl = Boundary(𝒯,tags="cylinderWalls")
  Γside = Boundary(𝒯,tags="walls")
  ncyl = get_normal_vector(Γcyl)
  nout = get_normal_vector(Γout)

  # Boundary conditions
  @unpack Re,D,ν = params
  @show U∞ = Re*ν/D
  @show Re
  Uₘ = 1.5U∞
  u₀(x,t) = VectorValue(0.0,0.0)
  u₀(t::Real) = x->u₀(x,t)
  uin((x,y),t) = VectorValue( 4 * Uₘ * y * ( H - y ) / ( (H^2) ), 0.0 ) #VectorValue(U∞,0.0)
  # uin((x,y),t) = VectorValue(U∞,0.0)
  uin(t::Real) = x -> uin(x,t)

  # Define FE spaces
  @unpack order = params
  reffeᵤ = ReferenceFE(lagrangian,VectorValue{2,Float64},order)
  reffeₚ = ReferenceFE(lagrangian,Float64,order-1)
  V = TestFESpace(Ω,reffeᵤ, conformity=:H1,
    dirichlet_tags=["inlet","walls","cylinderWalls"],
    dirichlet_masks=[(true,true),(true,true),(true,true)])
  U = TransientTrialFESpace(V, [uin,u₀,u₀])
  Q = TestFESpace(Ω,reffeₚ, conformity=:C0)
  P = TrialFESpace(Q)
  Κ = TestFESpace(Ω,reffeᵤ, conformity=:H1,
    dirichlet_tags=["inlet","walls","cylinderWalls"],
    dirichlet_masks=[(true,true),(true,true),(true,true)])
  Η = TrialFESpace(Κ,[u₀(0),u₀(0),u₀(0)])
  # mfs = BlockMultiFieldStyle(2,(1,2))
  # X = TransientMultiFieldFESpace([Η,U,P];style=mfs)
  # Y = MultiFieldFESpace([Κ,V,Q];style=mfs)
  X = TransientMultiFieldFESpace([Η,U,P])
  Y = MultiFieldFESpace([Κ,V,Q])
  X₀ = MultiFieldFESpace([U(0.0),P])
  Y₀ = MultiFieldFESpace([V,Q])

  # Measures
  degree = 2*order
  dΩ = Measure(Ω,degree)
  dΓcyl = Measure(Γcyl,degree)
  dΓout = Measure(Γout,degree)

  # Auxiliar forms
  c(a,u,v) = 0.5*((a⋅∇(u))⋅v - (a⋅∇(v))⋅u)

  # Stabilization Parameters
  c₁ = 4.0
  c₂ = 2.0
  cc = 1.0
  h2 = CellField(get_cell_measure(Ω),Ω)
  h = CellField(lazy_map(dx->dx^(1/2),get_cell_measure(Ω)),Ω)
  τₘ(u) = 1/(c₁*ν/h2 + c₂*((u⋅u).^(1/2))/h)
  τc(u) = cc * (h2/(c₁*τₘ(u)))


  # Define FE Operators
  a₀((u,p),(v,q)) = ∫( 2ν*(ε(u)⊙ε(v)) + (∇⋅u)*q - (∇⋅v)*p )dΩ
  l₀((v,q)) = ∫( 0.0*q )dΩ
  res(t,(η,u,p),(κ,v,q)) =
  ∫( ∂t(u)⋅v + 2ν*(ε(u)⊙ε(v)) + c(u,u,v) + (∇⋅u)*q - (∇⋅v)*p +
     τₘ(u)*((∇(u)'⋅u - η)⋅(∇(v)'⋅u - κ)) + τc(u)*((∇⋅u)*(∇⋅v)) )dΩ +
  ∫( 0.5*(u⋅nout)*(u⋅v) )dΓout
  op₀ = AffineFEOperator(a₀,l₀,X₀,Y₀)
  op = TransientFEOperator(res,X,Y)

  # Initial solution
  uₕₒ, pₕₒ = solve(op₀)
  xₕ₀ = interpolate_everywhere([u₀(0.0),uₕₒ,pₕₒ],X(0.0))
  ∂ₜxₕ₀ = interpolate_everywhere([u₀(0.0),u₀(0.0),0.0],X(0.0))
  filename = datadir(outFolder,"sol0")
  writevtk(Ω,filename,cellfields=["u"=>xₕ₀[2],"p"=>xₕ₀[3]],order=order)

  # Solver
  @unpack Δt,tf,ΔtOut,ρ∞ = params
  # ls = BlockPrecSolver(M_ls=LUSolver(),K_ls=LUSolver(),gmres_iter=1000,rtol=1.0e-8,verbose=false)
  # nls = NLSolver(ls,show_trace=true,method=:newton,iterations=10)
  nls = NLSolver(show_trace=true,method=:newton,iterations=10)
  ode_solver = GeneralizedAlpha(nls,Δt,ρ∞)
  # ode_solver = ThetaMethod(nls,Δt,1.0)

  # solution
  xₕₜ = solve(ode_solver,op,(xₕ₀,∂ₜxₕ₀),0.0,tf)
  # xₕₜ = solve(ode_solver,op,xₕ₀,0.0,tf)

  # Post-processing
  Fx = Float64[]
  Fy = Float64[]
  CD = Float64[]
  CL = Float64[]
  outMod = ceil(Int64, ΔtOut / Δt)
  filename = datadir(outFolder,"sol")
  forceFile = open( datadir(outFolder,"force_data.txt"), "w" )
  tick()
  createpvd(filename) do pvd
    cnt = 0
    for ((ηₕ,uₕ,pₕ),t) in xₕₜ
      cnt += 1
      Fxₜ,Fyₜ = ∑(∫(pₕ*ncyl-2ν*(ε(uₕ)⋅ncyl))dΓcyl)
      push!(Fx,Fxₜ)
      CDₜ = Fxₜ / (0.5 * D * U∞^2)
      push!(CD,CDₜ)
      push!(Fy,Fyₜ)
      CLₜ = Fyₜ  / (0.5 * D * U∞^2)
      push!(CL,CLₜ)
      println("Count = $cnt, t = $t, Fx = $Fxₜ, Fy = $Fyₜ, CD = $CDₜ, CL = $CLₜ")
      println(forceFile,
        "Count = $cnt, t = $t, Fx = $Fxₜ, Fy = $Fyₜ, CD = $CDₜ, CL = $CLₜ")

      if(cnt%outMod == 0)
        pvd[t] = createvtk(Ω,filename*"_"*@sprintf("%.5f",t)*".vtu",
          cellfields=["u"=>uₕ,"p"=>pₕ,"eta"=>ηₕ],order=order)
      end

      tock()
      println("----------")
      println()
      tick()
    end
  end

  close(forceFile)
  tock()

  return Fx,Fy,CD,CL

end
