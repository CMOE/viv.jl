module VIV
using Gridap
using GridapGmsh
using GridapDistributed
using GridapDistributed: DistributedTriangulation, DistributedCellField
using GridapPETSc
using PartitionedArrays
using Parameters
using DrWatson
using TickTock
using Printf
using GridapGmsh
using Gridap.MultiField
using Gridap.Algebra

include(srcdir("VIV_solvers/VIV_solvers.jl"))
using .VIVSolvers

"""
VIV_params

Parameters for the VIV.jl module.
"""
@with_kw struct VIV_params
  H::Float64 = 0.41 # Height of the channel
  L::Float64 = 2.2 # Length of the channel
  D::Float64 = 0.1 # Diameter of the cylinder
  Re::Float64 = 20.0 # Reynolds number
  ν::Float64 = 1.0e-6 # Kinematic viscosity
  mesh_file::String = "meshes/cylinder.msh" # Mesh file
  order::Int64 = 2 # Order of the finite elements
  Δt::Float64 = 0.01 # Time step
  tf::Float64 = 0.01 # Final time
  ΔtOut::Float64 = 10*Δt # vtk Out time
  ρ∞::Float64 = 0.5 # Spectral radius of the generalized alpha method
  # Uₘ::Float64 = 0.2 # Maximum inflow speed
  outFolder::String = "sims"
  test_name::String = "tmp"
  exec_path::String = "."
  #ρ::Float64 = 1000 # Density of water
end

include("VIV_serial.jl")
include("VIV_parallel.jl")

# Define solver options
options_mumps = "-snes_type newtonls \
-snes_linesearch_type basic  \
-snes_linesearch_damping 1.0 \
-snes_rtol 1.0e-8 \
-snes_atol 1.0e-10 \
-ksp_error_if_not_converged true \
-ksp_converged_reason -ksp_type preonly \
-pc_type lu \
-pc_factor_mat_solver_type mumps \
-mat_mumps_icntl_7 0"

function run_case(nprocs::Int,params)
  @unpack exec_path = params
  current_path = pwd()
  cd(exec_path)
  with_mpi() do distribute
    ranks = distribute_with_mpi(LinearIndices((nprocs,)))
    GridapPETSc.with(args=split(options_mumps)) do
      VIV.main(params,ranks)
    end
  end
  cd(current_path)
end

end # module
