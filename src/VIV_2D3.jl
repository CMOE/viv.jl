module VIV
using Gridap
using Parameters
using DrWatson
using GridapGmsh
"""
main()

Main function for VIV.jl. This function is called when the module is run as a script.
In this function we solve the Navier-Stokes equations for an oscillating cylinder in
cross-flow within a channel.

# Arguments
- `params::VIV_params`: Parameters for the simulation.

#Output
- time trace of forces on the cylinder
"""
function main(params)

  # Load mesh
  @unpack mesh_file = params
  @unpack H,L = params
  𝒯 = GmshDiscreteModel(mesh_file)

  # Triangulations
  Ω = Interior(𝒯)
  Γout = Boundary(𝒯,tags="outlet")
  Γcyl = Boundary(𝒯,tags="cylinderWalls")
  Γside = Boundary(𝒯,tags="walls")
  ncyl = get_normal_vector(Γcyl)
  nout = get_normal_vector(Γout)

  # Boundary conditions
  @unpack Re,D,ν,Uₘ = params
  @show U∞ = Re*ν/D
  @show Re
  u₀(x,t) = VectorValue(0.0,0.0)
  u₀(t::Real) = x->u₀(x,t)
  uin((x,y),t) = VectorValue( 4 * Uₘ * y * ( H - y ) * sin(π*t/8) / ( (H^2) ), 0.0 ) #VectorValue(U∞,0.0) 
  uin(t::Real) = x -> uin(x,t)

  # Define FE spaces
  @unpack order = params
  reffeᵤ = ReferenceFE(lagrangian,VectorValue{2,Float64},order)
  reffeₚ = ReferenceFE(lagrangian,Float64,order-1)
  V = TestFESpace(Ω,reffeᵤ, conformity=:H1,
    dirichlet_tags=["inlet","walls","cylinderWalls"],
    dirichlet_masks=[(true,true),(true,true),(true,true)])
  U = TransientTrialFESpace(V, [uin,u₀,u₀])
  Q = TestFESpace(Ω,reffeₚ, conformity=:C0)
  P = TrialFESpace(Q)
  Κ = TestFESpace(Ω,reffeᵤ, conformity=:H1,
    dirichlet_tags=["inlet","walls","cylinderWalls"],
    dirichlet_masks=[(true,true),(true,true),(true,true)])
  Η = TrialFESpace(Κ,[u₀(0),u₀(0),u₀(0)])
  X = TransientMultiFieldFESpace([U,P,Η])
  Y = MultiFieldFESpace([V,Q,Κ])
  X₀ = MultiFieldFESpace([U(0.0),P])
  Y₀ = MultiFieldFESpace([V,Q])

  # Measures
  degree = 2*order
  dΩ = Measure(Ω,degree)
  dΓcyl = Measure(Γcyl,degree)
  dΓout = Measure(Γout,degree)

  # Auxiliar forms
  c(a,u,v) = 0.5*((a⋅∇(u))⋅v - (a⋅∇(v))⋅u)

  # Stabilization Parameters
  c₁ = 4.0
  c₂ = 2.0
  cc = 1.0
  h2 = CellField(get_cell_measure(Ω),Ω)
  h = CellField(lazy_map(dx->dx^(1/2),get_cell_measure(Ω)),Ω)
  τₘ(u) = 1/(c₁*ν/h2 + c₂*((u⋅u).^(1/2))/h)
  τc(u) = cc * (h2/(c₁*τₘ(u)))


  # Define FE Operators 
  a₀((u,p),(v,q)) = ∫( ν*(ε(u)⊙ε(v)) + (∇⋅u)*q - (∇⋅v)*p )dΩ
  l₀((v,q)) = ∫( 0.0*q )dΩ
  res(t,(u,p,η),(v,q,κ)) =
    ∫( ∂t(u)⋅v + ν*(∇(u)⊙∇(v)) + c(u,u,v) + (∇⋅u)*q - (∇⋅v)*p +
       τₘ(u)*((∇(u)'⋅u - η)⋅(∇(v)'⋅u - κ)) + τc(u)*((∇⋅u)*(∇⋅v)) )dΩ +
    ∫( 0.5*(u⋅nout)*(u⋅v) )dΓout
  op₀ = AffineFEOperator(a₀,l₀,X₀,Y₀)
  op = TransientFEOperator(res,X,Y)

  # Initial solution
  uₕₒ, pₕₒ = solve(op₀)
  xₕ₀ = interpolate_everywhere([uₕₒ,pₕₒ,u₀(0.0)],X(0.0))
  ∂ₜxₕ₀ = interpolate_everywhere([u₀(0.0),0.0,u₀(0.0)],X(0.0))
  filename = datadir("sims","sol0")
  writevtk(Ω,filename,cellfields=["u"=>xₕ₀[1],"p"=>xₕ₀[2]],order=order)

  # Solver
  @unpack Δt,tf,ρ∞ = params
  nls = NLSolver(show_trace=true,method=:newton,iterations=3)
  # ode_solver = GeneralizedAlpha(nls,Δt,ρ∞)
  ode_solver = ThetaMethod(nls,Δt,1.0)

  # solution
  # xₕₜ = solve(ode_solver,op,(xₕ₀,∂ₜxₕ₀),0.0,tf)
  xₕₜ = solve(ode_solver,op,xₕ₀,0.0,tf)

  # Post-processing
  Fx = Float64[]
  Fy = Float64[]
  #CD = Float64[]
  #CL = Float64[]
  filename = datadir("sims","sol")
  createpvd(filename) do pvd
    for ((uₕ,pₕ,ηₕ),t) in xₕₜ
      pvd[t] = createvtk(Ω,filename*"_$t.vtu",cellfields=["u"=>uₕ,"p"=>pₕ,"eta"=>ηₕ],order=order)
      Fxₜ,Fyₜ = ∑(∫(pₕ*ncyl-ν*(ε(uₕ)⋅ncyl))dΓcyl)
      push!(Fx,Fxₜ)
      #CD = Fx / (0.5 * D * uₕ^(2))
      push!(Fy,Fyₜ)
      #CL = Fy  / (0.5 * D * uₕ^(2))
    end
  end


  return Fx,Fy

end

"""
VIV_params

Parameters for the VIV.jl module.
"""
@with_kw struct VIV_params
  H::Float64 = 0.41 # Height of the channel
  L::Float64 = 2.2 # Length of the channel
  D::Float64 = 0.1 # Diameter of the cylinder
  Re::Float64 = 20.0 # Reynolds number
  ν::Float64 = 1.0e-3 # Kinematic viscosity
  mesh_file::String = "meshes/cylinder.msh" # Mesh file
  order::Int64 = 2 # Order of the finite elements
  Δt::Float64 = 0.01 # Time step
  tf::Float64 = 0.01 # Final time
  ρ∞::Float64 = 0.95 # Spectral radius of the generalized alpha method
  Uₘ::Float64 = 0.2 # Maximum inflow speed
  #ρ::Float64 = 1000 # Density of water
end

end # module
