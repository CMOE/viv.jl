# Steps to compile VIV.jl

1. Install PETSc with MUMPS: in a terminal execute this code (you can save in a *.sh file and execute with `. FILENAME.sh`):

  1. Load modules: OpenMPI and CMAKE
  2. Execute installation script:
```
# Install PETSc
CURR_DIR=$(pwd)
PACKAGE=petsc
VERSION=3.18
INSTALL_ROOT=$HOME/bin
PETSC_INSTALL=$INSTALL_ROOT/$PACKAGE/$VERSION
TAR_FILE=$PACKAGE-$VERSION.tar.gz
URL="https://ftp.mcs.anl.gov/pub/petsc/release-snapshots/"
ROOT_DIR=/tmp
SOURCES_DIR=$ROOT_DIR/$PACKAGE-$VERSION
BUILD_DIR=$SOURCES_DIR/build
wget -q $URL/$TAR_FILE -O $ROOT_DIR/$TAR_FILE
mkdir -p $SOURCES_DIR
tar xzf $ROOT_DIR/$TAR_FILE -C $SOURCES_DIR --strip-components=1
cd $SOURCES_DIR
./configure --prefix=$PETSC_INSTALL --with-cc=mpicc --with-cxx=mpicxx --with-fc=mpif90 \
    --download-mumps --download-scalapack --download-parmetis --download-metis \
    --download-ptscotch --with-debugging --with-x=0 --with-shared=1 \
    --with-mpi=1 --with-64-bit-indices
make
make install
```
2. Adjust modules according to available modules in the machine
3. `source modules_CLUSTERNAME.sh`
4. Run julia to precompile packages
    
    - From the project root path execute: `julia --project=.`
    - In Pkg mode: `instantiate; build`
    - Check the `MPI` `.../build.log` and make sure it points to the system library loaded through the modules
    - Check the `GridapPETSc` `.../build.log` and make sure it points to the local installation of PETSc
    - In Pkg mode: `precompile`
    - exit julia

5. From the folder `compile` execute: `sbatch compile_CLUSTERNAME.sh`
6. Check that the `VIV.so` file is generated in the root folder
