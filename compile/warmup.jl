using VIV
using DrWatson
@quickactivate "VIV.jl"

mesh_file = datadir("meshes","cylinder_coarse.msh")
params = VIV.VIV_params(
  mesh_file=mesh_file,
  test_name="tmp",
  exec_path="/gpfs/scratch1/nodespecific/int6/"*ENV["USER"]*"/VIV.jl/VTKs/tmp",
  Re = 20,
  Δt=0.1,
  tf=0.2,
  Uₘ=0.3,
)
VIV.run_case(1,params)
