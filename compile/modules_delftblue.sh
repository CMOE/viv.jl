module load 2022r2 openmpi intel/oneapi mkl/2022.2.0
#module load 2022 OpenMPI/4.1.4-GCC-11.3.0 imkl/2022.1.0 Julia/1.8.2-linux-x86_64
export JULIA_MPI_BINARY=system
export JULIA_MPI_PATH=/apps/arch/2022r2/software/linux-rhel8-skylake_avx512/gcc-8.5.0/openmpi-4.1.1-fezcq73heq4rzzsbcumuq5xx4v5asv45
export JULIA_PETSC_LIBRARY=$HOME/progs/install/petsc/3.18/lib/libpetsc
