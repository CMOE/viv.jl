#!/bin/bash

#SBATCH --job-name="compile_VIV"
#SBATCH -p compute
#SBATCH -t 04:00:00
#SBATCH -n 1
#SBATCH -o stdout
#SBATCH -e stderr

source modules_delftblue.sh
mkdir ../data/tmp
mpiexecjl --project=../ -n 1 julia -O3 --check-bounds=no --color=yes compile.jl

